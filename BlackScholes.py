from math import sqrt, log, exp, erf
import random
import numpy as arange
import matplotlib.pyplot as plt
import pandas as pd
import os

S0 = 100  # Stocks price
strikes = [i for i in range(50, 150)]  # Exercise price range
T = 1  # Time to expiration
r = 0.01  # risk-free interest rate
q = 0.02  # dividend yield
vol = 0.2  # volatility
Nsteps = 100  # Number of step in MC

def montecarlobs(S0,K,T,r,q,vol,Nsteps):
    payoff = 0.0
    sfin=[]
    for i in range(Nsteps):
        rnd = random.gauss(0,sqrt(T))
        sfinal=S0*exp((r-q-0.5*vol**2)*T+vol*rnd)
        sfin.append(sfinal)
        payoff+=max(sfinal-K,0)
    payoff = payoff*exp(-r*T)/float(Nsteps)
    return payoff

