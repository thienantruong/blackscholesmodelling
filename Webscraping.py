from bs4 import BeautifulSoup
from urllib.request import urlopen
import pandas as pd
import os

wiki = 'https://en.wikipedia.org/w/index.php?title=List_of_Belgian_beers&diff=829421783&oldid=825178994'
page = urlopen(wiki)
soup = BeautifulSoup(page, 'lxml')
Brand = []
Sort = []
Method = []
Percentage = []
Brewery = []

tables =soup.findAll('table',{'class':'wikitable sortable'})

for table in tables:
    for row in table.findAll('tr'):
        cells = row.findAll('td')
        if len(cells) == 5:
            Brand.append(cells[0].find(text=True))
            Sort.append(cells[1].find(text=True))
            Method.append(cells[2].find(text=True))
            Percentage.append(cells[3].find(text=True))
            Brewery.append(cells[4].find(text=True))
        elif len(cells) ==4:
            Brand.append(cells[0].find(text=True))
            Sort.append(cells[1].find(text=True))
            Method.append('')
            Percentage.append(cells[2].find(text=True))
            Brewery.append(cells[3].find(text=True))
        elif len(cells) > 0:
            print(cells)

    df_beers = pd.DataFrame({"Brand": Brand,
                             "Sort": Sort,
                             "Method": Method,
                             "Percentage": Percentage,
                             "Brewery": Brewery})
df_beers.dtypes

def tryconvert(x):
    try:
        if x != None:
            return float(x.strip("% ()"))
        else:
            return 0
    except:
        return 0

df_beers['Percentage_new'] = df_beers['Percentage'].map(tryconvert)

# print out all
